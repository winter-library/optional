#include <iostream>
#include <cassert>

#include "../../include/optional/optional.hpp"

void ofInputAbstractValuePointer(){
	class Test{ public: int const x = 10; };
	assert(Optional<Test *>::of(new Test()).get().x == 10);
	std::cout << "[TEST]::[ofInputAbstractValuePointer]::SUCCESS" << std::endl;
}

void ofInputAbstractValuePointerNull(){
	class Test{ public: int const x = 10; };
	try{
		Optional<Test *>::of(nullptr);
		assert(false);
	}catch (const std::invalid_argument &exception){
		assert(true);
		std::cout << "[TEST]::[ofInputAbstractValuePointerNull]::SUCCESS::" << exception.what() << std::endl;
	}
}

void ofInputPrimitiveValuePointer(){
	assert(Optional<int *>::of(new int(10)).get() == 10);
	assert(Optional<float *>::of(new float(2.5)).get() == 2.5);
	assert(Optional<char *>::of(new char('a')).get() == 'a');
	assert(Optional<double *>::of(new double(5.5)).get() == 5.5);
	assert(Optional<std::string *>::of(new std::string("ola")).get() == "ola");
	assert(Optional<bool *>::of(new bool(true)).get());
	std::cout << "[TEST]::[ofInputPrimitiveValuePointer]::SUCCESS" << std::endl;
}

void ofInputPrimitiveValuePointerNull(){
	try{
		Optional<int *>::of(nullptr);
		assert(false);
	}catch (const std::invalid_argument &exception){
		assert(true);
		std::cout << "[TEST]::[ofInputPrimitiveValuePointerNull]::SUCCESS::" << exception.what() << std::endl;
	};
}

void ofInputAbstractValue(){
	class Test{ public: int const x = 10; };
	assert(Optional<Test>::of(Test()).get().x == 10);
	std::cout << "[TEST]::[ofInputAbstractValue]::SUCCESS" << std::endl;
}

void ofInputPrimitiveValue(){
	assert(Optional<int>::of(10).get() == 10);
	assert(Optional<float>::of(2.5).get() == 2.5);
	assert(Optional<char>::of('a').get() == 'a');
	assert(Optional<double>::of(5.5).get() == 5.5);
	assert(Optional<std::string>::of("ola").get() == "ola");
	assert(Optional<bool>::of(true).get());
	std::cout << "[TEST]::[ofInputPrimitiveValue]::SUCCESS" << std::endl;
}
