#include <iostream>
#include <cassert>

#include "../../include/optional/optional.hpp"

void isEmpty(){
    class Test{ public: int const x = 10; };
    assert(Optional<int *>::ofNullable(nullptr).isEmpty());
    assert(Optional<Test *>::empty().isEmpty());
    std::cout << "[TEST]::[isEmpty]::SUCCESS" << std::endl;
}

void isNotEmpty(){
    class Test{ public: int const x = 10; };
    assert(!Optional<int *>::ofNullable(new int(10)).isEmpty());
    assert(!Optional<Test *>::ofNullable(new Test()).isEmpty());
    std::cout << "[TEST]::[isNotEmpty]::SUCCESS" << std::endl;
}