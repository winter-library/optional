#include <iostream>
#include <cassert>

#include "../../include/optional/optional.hpp"

void isPresent(){
    class Test{ public: int const x = 10; };
    assert(Optional<int *>::of(new int(10)).isPresent());
    assert(Optional<Test *>::of(new Test()).isPresent());
    std::cout << "[TEST]::[isPresent]::SUCCESS" << std::endl;
}

void isNotPresent(){
    assert(!Optional<int *>::ofNullable(nullptr).isPresent());
    assert(!Optional<float *>::empty().isPresent());
    std::cout << "[TEST]::[isNotPresent]::SUCCESS" << std::endl;
}