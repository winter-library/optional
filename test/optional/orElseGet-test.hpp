#include <iostream>
#include <cassert>

#include "../../include/optional/optional.hpp"

void orElseGetPrimitiveValueProviderNull(){
	try{
		Optional<int>::of(10).orElseGet(nullptr);
		assert(false);
	}catch (const std::invalid_argument &exception){
		assert(true);
		std::cout << "[TEST]::[orElseGetPrimitiveValueProviderNull]::SUCCESS::" << exception.what() << std::endl;
	};
}

void orElseGetPrimitiveValue(){
	assert(Optional<int>::of(10).orElseGet([]() { return 20; }) == 20);
	assert(Optional<float>::of(2.5).orElseGet([]() { return 5.5; }) == 5.5);
	assert(Optional<char>::of('a').orElseGet([]() { return 'b'; }) == 'b');
	assert(Optional<double>::of(5.5).orElseGet([]() { return 10.5; }) == 10.5);
	assert(Optional<std::string>::of("ola").orElseGet([]() { return "oi"; }) == "oi");
	assert(Optional<bool>::of(false).orElseGet([]() { return true; }));
	std::cout << "[TEST]::[orElseGetPrimitiveValue]::SUCCESS" << std::endl;
}

void orElseGetPrimitiveValueReturningPrimitiveProviderNull(){
	try{
		Optional<int *>::of(new int(10)).orElseGet(nullptr);
		assert(false);
	}catch (const std::invalid_argument &exception){
		assert(true);
		std::cout << "[TEST]::[orElseGetPrimitiveValueReturningPrimitiveProviderNull]::SUCCESS::" << exception.what() << std::endl;
	};
}

void orElseGetPointerPrimitiveValueReturningPrimitiveProviderNull(){
	try{
		Optional<int *>::of(new int(10)).orElseGetPointer(nullptr);
		assert(false);
	}catch (const std::invalid_argument &exception){
		assert(true);
		std::cout << "[TEST]::[orElseGetPointerPrimitiveValueReturningPrimitiveProviderNull]::SUCCESS::" << exception.what() << std::endl;
	};
}

void orElseGetPrimitiveValueReturningPrimitive(){
	assert(Optional<int *>::of(new int(10)).orElseGet([]() { return 20; }) == 10);
	assert(Optional<float *>::of(new float(2.5)).orElseGet([]() { return 5.5; }) == 2.5);
	assert(Optional<char *>::of(new char('a')).orElseGet([]() { return 'b'; }) == 'a');
	assert(Optional<double *>::of(new double(5.5)).orElseGet([]() { return 10.5; }) == 5.5);
	assert(Optional<std::string *>::ofNullable(nullptr).orElseGet([]() { return "oi"; }) == "oi");
	assert(Optional<bool *>::empty().orElseGet([]() { return true; }));
	std::cout << "[TEST]::[orElseGetPrimitiveValueReturningPrimitive]::SUCCESS" << std::endl;
}

void orElseGetPrimitiveValueReturningPointer(){
	int const *const valor = new int(20);
	assert(*Optional<int *>::of(new int(10)).orElseGetPointer([&valor]() { return valor; }) == 10);
	assert(*Optional<int *>::ofNullable(nullptr).orElseGetPointer([&valor]() { return valor; }) == 20);
	assert(*Optional<int *>::empty().orElseGetPointer([&valor]() { return valor; }) == 20);
	delete valor;
	std::cout << "[TEST]::[orElseGetPrimitiveValueReturningPointer]::SUCCESS" << std::endl;
}

void orElseGetAbstractValueProviderNull(){
	class Test{ public: int const x = 10; };
	try{
		Optional<Test *>::of(new Test()).orElseGet(nullptr);
		assert(false);
	}catch (const std::invalid_argument &exception){
		assert(true);
		std::cout << "[TEST]::[orElseGetAbstractValueProviderNull]::SUCCESS::" << exception.what() << std::endl;
	};
}

void orElseGetPointerAbstractValueProviderNull(){
	class Test{ public: int const x = 10; };
	try{
		Optional<Test *>::of(new Test()).orElseGetPointer(nullptr);
		assert(false);
	}catch (const std::invalid_argument &exception){
		assert(true);
		std::cout << "[TEST]::[orElseGetPointerAbstractValueProviderNull]::SUCCESS::" << exception.what() << std::endl;
	};
}

void orElseGetAbstractValueReturningAbstract(){
	class Test{ 
		private:
			int const x;
		public:
			explicit Test(int const x) : x(x) {}
			int const get() const { return x; }
	};
	assert(Optional<Test *>::of(new Test(10)).orElseGet([]() { return Test(20); }).get() == 10);
	assert(Optional<Test *>::ofNullable(nullptr).orElseGet([]() { return Test(20); }).get() == 20);
	assert(Optional<Test *>::empty().orElseGet([]() { return Test(20); }).get() == 20);
	std::cout << "[TEST]::[orElseGetAbstractValueReturningAbstract]::SUCCESS" << std::endl;
}

void orElseGetAbstractValueReturningPointer(){
	class Test{
		private:
			int const x;
		public:
			explicit Test(int const x) : x(x) {}
			int const get() const { return x; }
	};
	Test const *const valor = new Test(20);
	assert(Optional<Test *>::of(new Test(10)).orElseGetPointer([&valor]() { return valor; })->get() == 10);
	assert(Optional<Test *>::ofNullable(nullptr).orElseGetPointer([&valor]() { return valor; })->get() == 20);
	assert(Optional<Test *>::empty().orElseGetPointer([&valor]() { return valor; })->get() == 20);
	delete valor;
	std::cout << "[TEST]::[orElseGetAbstractValueReturningPointer]::SUCCESS" << std::endl;
}